
/**
 * Required External Modules
 */

import express, { Express, Request, Response } from 'express'
import dotenv from 'dotenv'
import cors from 'cors'
import helmet from 'helmet'
import { prisma } from './utils/prisma.js'
import { apiRouter } from './api/index.js'

dotenv.config()


/**
 * App Variables
 */
const PORT: number = parseInt(process.env.PORT as string, 10) || 3000
const app: Express = express()

/**
 *  App Configuration
 */

app.use(cors())
app.use(helmet())
app.use(express.json())


/**
 * Server Activation
 */

app.listen(PORT, () => {
    console.log(`⚡️[server]: SERVER is running at https://localhost:${PORT}`)
})


/**
 * others
 */

app.post('/post', async (req: Request, res: Response) => {
    const { title, content, authorEmail } = req.body
    const post = await prisma.post.create({
        data: {
            title,
            content,
            published: false,
            author: { connect: { email: authorEmail } }
        }
    })
    res.json(post)
})

app.get('/', (res: Response) => {
    res.send('Sahaware Assessment Backend API')
})

app.use('/api/v1', apiRouter)




