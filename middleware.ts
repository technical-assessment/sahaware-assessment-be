import { NextFunction, Request, Response } from "express"
import jwt, { JwtPayload, Secret, TokenExpiredError } from "jsonwebtoken"
import { Logger } from "./utils/logger"

const logger = new Logger()

export function isAuthenticated(req: Request, res: Response, next: NextFunction) {
    const authorization = req.headers.authorization

    if (!authorization) {
        logger.error(res, '🚫 Un-Authorized 🚫', 401)
        return
    }

    try {
        const token = authorization.split(' ')[1];
        const payload: JwtPayload = <JwtPayload>jwt.verify(token, <Secret>process.env.JWT_ACCESS_SECRET)
        req.params = payload;
    } catch (err) {
        if (err instanceof TokenExpiredError) {
            logger.error(res, 'Your login session has expired', 400)
            return
        }
        logger.error(res, '🚫 Un-Authorized 🚫', 401)
        return
    }

    return next();
}