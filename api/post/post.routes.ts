import express from 'express'
import { isAuthenticated } from '../../middleware'
import { Logger } from "../../utils/logger"
import { createPost, findAllPosts } from './post.service'

export const postRouter = express.Router()
const logger = new Logger()

postRouter.post('/', isAuthenticated, async (req, res, next) => {
    try {
        const { userId } = req.params
        const {
            title,
            description,
            content,
            thumbnail,
            published
        } = req.body

        if (!title) {
            logger.error(res, 'Title should not be empty.', 400)
            return
        }
        console.log({
            title,
            description,
            content,
            thumbnail,
            published,
            authorId: userId
        })
        const post = await createPost({
            title,
            description,
            content,
            thumbnail,
            published,
            authorId: userId
        })


        res.json({
            status: '200',
            message: 'New post successfully created.',
            data: post
        })
    } catch (err) {
        if (err instanceof Error) {
            logger.error(res, err.message, 400)
        } else {
            next(err)
        }
    }
})

postRouter.get('/', isAuthenticated, async (_req, res, next) => {
    const posts = await findAllPosts()

    try {
        res.json({
            status: '200',
            message: 'Posts successfully fetched.',
            data: posts
        })
    } catch (err) {
        if (err instanceof Error) {
            logger.error(res, err.message, 400)
        } else {
            next(err)
        }
    }
})
