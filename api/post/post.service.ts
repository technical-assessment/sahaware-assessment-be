import { prisma as db } from '../../utils/prisma.js'


export function findPostById(id: number) {
    return db.post.findUnique({
        where: {
            id,
        },
    })
}

export function findAllPosts() {
    return db.post.findMany()
}

export function createPost(post: {
    title: string,
    description: string,
    content: string,
    thumbnail: string,
    published: boolean,
    authorId: string,
    // categories: string[] // TODO: add this categories later on
}) {
    return db.post.create({
        data: post
    })
}

export function publishPost(id: number) {
    return db.post.update({
        where: {
            id,
        },
        data: {
            published: true
        }
    })
}