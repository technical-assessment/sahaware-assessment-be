import express from 'express'
import bcrypt from 'bcrypt'
import jwt, { JsonWebTokenError, JwtPayload, Secret } from 'jsonwebtoken'

import { v4 } from 'uuid';
import { generateTokens } from '../../utils/jwt.js'
import { addRefreshTokenToWhitelist, deleteRefreshToken, findRefreshTokenById, revokeTokens } from './auth.service'
import {
    findUserByEmail,
    createUserByEmailAndPassword,
    findUserById,
} from '../users/users.service'
import { Logger } from '../../utils/logger.js'
import { hashToken } from '../../utils/hashToken.js';
import { User } from '@prisma/client';

export const authRouter = express.Router()
const logger = new Logger()

authRouter.post('/register', async (req, res, next) => {
    try {
        const { fullname, email, password } = req.body
        if (!fullname) {
            logger.error(res, 'Full Name should not be empty.', 400)
            return
        }

        let existingUser: User | null
        if (!email || !password) {
            logger.error(res, 'You must provide an email and a password.', 400);
            return
        } else {
            existingUser = await findUserByEmail(email);
        }

        if (existingUser) {
            logger.error(res, 'Email already in use.', 400)
            return
        }

        const user = await createUserByEmailAndPassword({
            fullname, email, password
        })

        const jti = v4()
        const { accessToken, refreshToken } = generateTokens(user, jti)
        await addRefreshTokenToWhitelist({ jti, refreshToken, userId: user.id })

        res.json({
            accessToken,
            refreshToken,
        })
    } catch (err) {
        next(err)
    }
})

authRouter.post('/login', async (req, res, next) => {
    try {
        const { email, password } = req.body

        let existingUser: User | null
        let validPassword: boolean | null
        if (!email || !password) {
            logger.error(res, 'You must provide an email and a password.', 400)
            return
        } else {
            existingUser = await findUserByEmail(email);
        }

        if (!existingUser) {
            logger.error(res, 'Invalid login credentials.', 400)
            return
        } else {
            validPassword = await bcrypt.compare(password, existingUser.password)
        }

        if (!validPassword) {
            logger.error(res, 'Invalid login credentials.', 400)
            return
        }

        const jti = v4()
        const { accessToken, refreshToken } = generateTokens(existingUser, jti)
        await addRefreshTokenToWhitelist({ jti, refreshToken, userId: existingUser.id })

        res.json({
            accessToken,
            refreshToken,
        })
    } catch (err) {
        next(err)
    }
})

authRouter.post('/refreshToken', async (req, res, next) => {
    try {
        const { refreshToken } = req.body
        if (!refreshToken) {
            logger.error(res, 'Missing refresh token.')
            return
        }

        const payload: JwtPayload = <JwtPayload>jwt.verify(refreshToken, <Secret>process.env.JWT_REFRESH_SECRET)

        const savedRefreshToken = await findRefreshTokenById(<string>payload.jti)
        if (!savedRefreshToken || savedRefreshToken.revoked === true) {
            logger.error(res, 'Unauthorized', 401)
            return
        }

        const hashedToken = hashToken(refreshToken)
        if (hashedToken !== savedRefreshToken.hashedToken) {
            logger.error(res, 'Unauthorized', 401)
            return
        }


        const user = await findUserById(<string>payload.userId)
        if (!user) {
            logger.error(res, 'Unauthorized', 401)
            return
        }

        await deleteRefreshToken(savedRefreshToken.id);
        const jti = v4();
        const { accessToken, refreshToken: newRefreshToken } = generateTokens(user, jti);
        await addRefreshTokenToWhitelist({ jti, refreshToken: newRefreshToken, userId: user.id });

        res.json({
            accessToken,
            refreshToken: newRefreshToken,
        })
    } catch (err) {
        if (err instanceof JsonWebTokenError) {
            logger.error(res, 'Invalid Token', 400)
        } else {
            next(err)
        }
    }
})

authRouter.post('/revokeRefreshTokens', async (req, res, next) => {
    try {
        const { userId } = req.body

        let existingUser: User | null
        if (!userId) {
            logger.error(res, 'You must provide an user id.', 400)
            return
        } else {
            existingUser = await findUserById(userId);
        }

        if (!existingUser) {
            logger.error(res, `Invalid user with id #${userId}`, 400)
            return
        }

        await revokeTokens(userId)
        const message = `Tokens revoked for user with id #${userId}`
        res.json({ message })
    } catch (err) {
        next(err)
    }
})
