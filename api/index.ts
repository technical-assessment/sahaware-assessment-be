import express from "express"
import { authRouter } from './auth/auth.routes'
import { userRouter } from "./users/users.routes"
import { categoryRouter } from "./category/category.routes"
import { postRouter } from "./post/post.routes"

export const apiRouter = express.Router()

apiRouter.use('/auth', authRouter)
apiRouter.use('/users', userRouter)
apiRouter.use('/categories', categoryRouter)
apiRouter.use('/posts', postRouter)
