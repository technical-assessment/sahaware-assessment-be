import express from 'express'
import { Logger } from "../../utils/logger"
import { Category } from '@prisma/client'
import { fineCategoryByName, createCategory, findAllCategories } from './category.service'
import { isAuthenticated } from '../../middleware'

export const categoryRouter = express.Router()
const logger = new Logger()

categoryRouter.post('/', isAuthenticated, async (req, res, next) => {
    try {
        const { name } = req.body

        let existingCategory: Category | null
        if (!name) {
            logger.error(res, 'Name should not be empty.', 400)
            return
        } else {
            existingCategory = await fineCategoryByName(name)
        }

        if (existingCategory) {
            logger.error(res, 'Category name already in use, please use another name.', 400)
            return
        }

        const category = await createCategory({
            name
        })


        res.json({
            status: '200',
            message: 'New category successfully created.',
            data: category
        })
    } catch (err) {
        if (err instanceof Error) {
            logger.error(res, err.message, 400)
        } else {
            next(err)
        }
    }
})

categoryRouter.get('/', isAuthenticated, async (_req, res, next) => {
    const categories = await findAllCategories()

    try {
        res.json({
            status: '200',
            message: 'Categories successfully fetched.',
            data: categories
        })
    } catch (err) {
        if (err instanceof Error) {
            logger.error(res, err.message, 400)
        } else {
            next(err)
        }
    }
})
