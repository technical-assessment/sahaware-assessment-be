import { prisma as db } from '../../utils/prisma.js'


export function findAllCategories() {
    return db.category.findMany()
}

export function fineCategoryByName(name: string) {
    return db.category.findUnique({
        where: {
            name,
        }
    })
}

export function createCategory(category: { name: string }) {
    return db.category.create({
        data: category
    })
}