import bcrypt from 'bcrypt'
import { prisma as db } from '../../utils/prisma.js'


export function findUserById(id: string) {
    return db.user.findUnique({
        where: {
            id,
        },
    });
}

export function findUserByEmail(email: string) {
    return db.user.findUnique({
        where: {
            email,
        },
    });
}

export function createUserByEmailAndPassword(user: { fullname: string, email: string, password: string; }) {
    user.password = bcrypt.hashSync(user.password, 12);
    return db.user.create({
        data: user,
    });
}