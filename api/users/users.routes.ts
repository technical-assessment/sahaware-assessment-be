import { User } from '@prisma/client';
import express from 'express'
import { isAuthenticated } from '../../middleware';
import { findUserById } from './users.service';

export const userRouter = express.Router()

userRouter.get('/profile', isAuthenticated, async (req, res, next) => {
    try {
        const { userId } = req.params;
        const user: User | null = await findUserById(userId)
        const userObj = {
            id: user?.id,
            fullname: user?.fullname,
            email: user?.email
        }
        res.json(userObj);
    } catch (err) {
        next(err);
    }
});