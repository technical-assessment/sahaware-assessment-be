import jwt, { Secret } from "jsonwebtoken"

const JWT_ACCESS: Secret = <Secret>process.env.JWT_ACCESS_SECRET
const JWT_REFRESH: Secret = <Secret>process.env.JWT_REFRESH_SECRET

// Token is between 5 minutes - 15 minutes usually
export function generateAccessToken(user: { id: any; }) {
    return jwt.sign({ userId: user.id }, JWT_ACCESS, {
        expiresIn: '5m',
    })
}

// 8h set to make the user login again each day.
// But keep user logged in if he is using the app.
// Should be maximum of 7 days, and make user login again after 7 days of inactivity.
export function generateRefreshToken(user: { id: any; }, jti: any) {
    return jwt.sign({
        userId: user.id,
        jti
    }, JWT_REFRESH, {
        expiresIn: '8h',
    })
}

export function generateTokens(user: any, jti: any) {
    const accessToken = generateAccessToken(user)
    const refreshToken = generateRefreshToken(user, jti)

    return {
        accessToken,
        refreshToken,
    }
}
