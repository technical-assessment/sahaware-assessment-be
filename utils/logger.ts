import { Response } from "express"
import pino from "pino"

const logger = pino({
    transport: {
        target: "pino-pretty",
        options: {
            translateTime: "SYS:dd-mm-yy HH:MM:ss",
            ignore: "pid,hostname"
        }
    }
})

export class Logger {
    error(res: Response, message?: string, status?: number) {
        let resStatus = status || 500
        let resMessage = message || 'Please contact your system administrator.'
        res.status(resStatus).json({
            status: resStatus,
            message: resMessage
        })
        logger.error(resMessage)
    }

    info(message?: string, res?: Response,) {
        let resMessage = message || 'Success'
        if (res) res.status(200).send(resMessage)
        logger.info(resMessage)
    }
}